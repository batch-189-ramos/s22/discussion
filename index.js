// alert("Hello!")

// ARRAY METHODS

/*
1. Mutator Methods
	- mutator methods are function taht mutate or change an array after they are created. These methods manipulate original array performing various tasks such as adding or removing elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

/*
	push ()
		- Add an element in the end of an array and return's the array's length

	Syntax:
		arrayName.push(element)
*/

console.log("Current Array:")
console.log(fruits)

// Adding element/s
let fruitsLength = fruits.push("Mango"); //stored in a variable
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log(fruits);

/*
	pop ()
		- removes the last element in our array and returns the removed element (when applied inside a variable)

	Syntax:
		arrayName.pop()

*/



let removedFruit = fruits.pop()
console.log(removedFruit)
console.log("Mutated array from the pop method:")
console.log(fruits)

/*
	unshift()
		- adds one or more elements at the beginning of an array
		returns the length of the array (when presented inside a varibale)
	Syntax:
		arrayName.unshift(elementA)
		arrayName.unshift(elementA, element B)

*/

fruits.unshift("Lime", "Banana")
console.log("Mutated array from unshift method")
console.log(fruits)

/*
	shift()
		- remove an elements at the beginning of our array and returns the removed element

	Syntax:
		arrayName.shift()

*/

let anotherFruit = fruits.shift()
console.log(anotherFruit);
console.log("Mutated array from shift method")
console.log(fruits)


/*
	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element

	Syntax:
		arrayName.splic(startingIndex, deleteCount, elementsToBeAdded)	

*/

let fruitsSplice = fruits.splice(1, 2, "Lime", "Cherry")
console.log("Mutated arrays from splice method")
console.log(fruits)
console.log(fruitsSplice)

/*
	sort()
		- rearranges the array elements in aplhanumeric order

	Syntax:
		arrayName.sort();

*/

fruits.sort()
console.log("Mutated array from sort method")
console.log(fruits)


//=========================

let mixedArray = [50, 14, 100, 1000, "Carlos", "Nej", "Bernard", "Charles"]
console.log(mixedArray.sort())



/*
	for sorting the items in descending order:
		fruits.sort().reverse()
		console.log(fruits)

*/

/*
	reverse()
		- reverses the order of array elements

	Syntax:
		arrayName.reverse();
*/

fruits.reverse()
console.log("Mutated array from reverse method")
console.log(fruits)

/*
	Non-mutator Methods
		- These are functions or methods that do not modify or change an array after they are created. These methods do not manipulate the original array performing various tasks such us returning elements from an array and combining arrays and printing the output

	
*/

let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "DE"]
console.log(countries)


/*
	indexOf()
		- returns the index number of the first matching element found in an array. If no match was found, the result will be -1. The search process will be done from our first element proceeding to the last element.

	Syntax:
		arrayName.indexOf(searchValue)
		arrayName.inderOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf("PH")
console.log("Result of indexOf method: " + firstIndex)
firstIndex = countries.indexOf("PH", 4)
console.log("Result of indexOf method: " + firstIndex)
firstIndex = countries.indexOf("PH", 7)
console.log("Result of indexOf method: " + firstIndex)
firstIndex = countries.indexOf("PH", -1)
console.log("Result of indexOf method: " + firstIndex)

/*
	lastIndexOf()
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first element

	Syntax;
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(SearchValue, fromIndex)

*/

let lastIndex = countries.lastIndexOf("PH")
console.log("Result of lastIndexOf: " + lastIndex)

let lastIndexStart = countries.lastIndexOf("PH", 4)
console.log("Result of lastIndexOf: " + lastIndexStart)

/*
	slice()
		- portions /slices elements from our array and returns the new array

	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex)

*/

console.log(countries)
let slicedArrayA = countries.slice(2);
console.log("Result from slice method:")
console.log(slicedArrayA)
console.log(countries)

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:")
console.log(slicedArrayB)

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:")
console.log(slicedArrayC)

/*
	toString()
		- returns an array as a string separated by commas

	Syntax:
		arrayName.toString()

*/

let stringArray = countries.toString()
console.log("Result from toString method")
console.log(stringArray)

/*
	concat()
		- combines two arrays and returns the combined results

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)

*/

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breath sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method")
console.log(tasks)


// Combining Multiple Arrays

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks)


// Combining arrays with elements

let combinesTasks = taskArrayA.concat("smell express", "throw react")
console.log(combinesTasks)

/*
	join()
		- returns an array as a string separator by specified separator string

	Syntax: 
		arrayName.join(separatorString)

*/

let students = ["Tristan", "Bernard", "Carlos", "Nehemiah"]
console.log(students.join())
console.log(students.join(' '))
console.log(students.join(" - "))



/*
	Iteration Methods are loops designed to perform repitive tasks on arrays. Useful for manipulating array data resulting in complex tasks.
*/


/*
	forEach()
		- similar to for loop that iterated on each array element
	
	Syntax:
		arrayName.forEach( function(individualElement) {
			statement
		})

*/


allTasks.forEach(function(task){
	console.log(task)
})


// Using forEach with conditional statements

let filteredTasks = [];

allTasks.forEach(function(task){
	console.log(task)
	if(task.length > 10) {
		filteredTasks.push(task)
	}
})


console.log(allTasks)
console.log("Result of filteredTasks:")
console.log(filteredTasks)


/*
	map()
		- iterates on each elements and returns new array with different values depending on the result of the function's operation

	Syntax:
		let/const resultArray = arrayName.map(function(individualElement) {
			statement
		})
	
*/


let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number) {
	console.log(number)
	return number * number
})


console.log("Original Array:")
console.log(numbers)
console.log("Result of map method:")
console.log(numberMap)

/*
	every()
		- checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise.

	Syntax:
		let/const resultArray = arrayName.every(function(individualElement) {
			return expression/condition
		})
*/


let allValid = numbers.every(function(number) {
	return (number < 3);
})


// let numbers = [1, 2, 3, 4, 5]
console.log("Result of every method:");
console.log(allValid)


/*
	some()
		- checks if at least one element in the array meet the given condition. Returns a true value if at least one element meets the given condition and false if otherwise

	Syntax:
		let/const resultArray = arrayName.some(function(individualElement) {
			return expression/condition
		})
*/

let someValid = numbers.some(function(number) {
	return (number , 2)
})

console.log("Result of some method:");
console.log(someValid)


/*
	filter()
		- returns a new array that contains elements which meets the given condition. Return an empty array if no elements were found (that satisfied the given condition)

	Syntax:
		let/const resultArray = arrayName.filter(function(individualElement) {
			return expression/condition
		})
*/

let filterValid = numbers.filter(function(number) {
	return	(number < 3)
})

console.log("Result of filter method:");
console.log(filterValid)

let nothingFound = numbers.filter(function(number){
	return (number == 0)
})

console.log("Result of filter method:");
console.log(nothingFound)

// Filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3) {
		filteredNumbers.push(number)
	}
})

console.log("Result of filtering using forEach method:");
console.log(filteredNumbers)


/*
	includes method
		- methods can be "chained" using them one after another. The result of the first method is used on the second method until all "chained" methods have been resolved.


*/

let products = ["Mouse","KeyboArd", "Laptop", "Monitor"]

let filteredProdcts = products.filter(function(product) {
	return product.toLowerCase().includes("a")
})

console.log("Result of includes method:");
console.log(filteredProdcts)



